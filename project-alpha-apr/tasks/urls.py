from django.urls import path
from tasks.views import create_task, view_tasks


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("view/", view_tasks, name="view_tasks"),
]
